django-ldapdb (1.5.1-3) unstable; urgency=medium

  * Team upload.

  * Adding patches from patch queue branch (Closes: #994257)
    Picking some patches from the upstream development that makes the
    autopkgtest working again.
  * d/control: Update Standards-Version to 4.6.0

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 05 Nov 2021 17:54:09 +0100

django-ldapdb (1.5.1-2) unstable; urgency=medium

  * Run manage_dev.py for CI tests (Closes: #972874).

 -- Michael Fladischer <fladi@debian.org>  Sun, 25 Oct 2020 20:43:12 +0100

django-ldapdb (1.5.1-1) unstable; urgency=low

  * Team upload.

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Michael Fladischer ]
  * Add debian/gbp.conf.
  * New upstream release.
  * Bump debhelper version to 13.
  * Disable tests through PYBUILD_DISABLE.
  * Set Rules-Requires-Root: no.
  * Clean up django_ldapdb.egg-info/requires.txt to allow two builds in
    a row.

 -- Michael Fladischer <fladi@debian.org>  Fri, 16 Oct 2020 21:13:28 +0200

django-ldapdb (1.4.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Pierre-Elliott Bécue ]
  * New upstream release 1.4.0.
  * d/control:
    - Raised debhelper compatibility level to 12
    - Bump Standards-Version to 4.4.0. No change required.

 -- Pierre-Elliott Bécue <peb@debian.org>  Sat, 20 Jul 2019 23:29:18 +0200

django-ldapdb (1.3.0-1) unstable; urgency=medium

  * New upstream release 1.3.0
  * d/control:
    - Replace my crans address by my debian address
    - Bump Standards-Version to 4.2.1.

 -- Pierre-Elliott Bécue <peb@debian.org>  Sat, 22 Dec 2018 22:37:34 +0100

django-ldapdb (1.2.0-1) unstable; urgency=medium

  * New upstream release 1.2.0
  * Drop the DS rebuild as upstream included the required files to run the
    tests properly \o/
  * Drop d/patches as upstream fixed the dependencies
  * d/control:
    - Bump Standards-Version to 4.1.5. No change required.

 -- Pierre-Elliott Bécue <becue@crans.org>  Wed, 01 Aug 2018 18:45:37 +0200

django-ldapdb (1.0.0+ds-1) unstable; urgency=low

  [ Pierre-Elliott Bécue ]
  * Re-packaging (Closes: #898750)
  * DS build in order to include tests files

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/rules: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field

 -- Pierre-Elliott Bécue <becue@crans.org>  Sat, 19 May 2018 16:56:34 +0200

django-ldapdb (0.2.0-1~bpo70+2) wheezy-backports; urgency=medium

  * Rebuild for correct version of the Python dependencies (with the right
    DIST for cowbuilder).

 -- Olivier Berger <obergix@debian.org>  Mon, 13 Jan 2014 16:54:46 +0100

django-ldapdb (0.2.0-1~bpo70+1) wheezy-backports; urgency=low

  * Rebuild for wheezy-backports.
  * Deactivate tests running during build as python-mockldap is not in stable

 -- Olivier Berger <obergix@debian.org>  Tue, 07 Jan 2014 18:26:28 +0100

django-ldapdb (0.2.0-1) unstable; urgency=low

  * Initial packaging (Closes: #730548).

 -- Olivier Berger <obergix@debian.org>  Wed, 04 Dec 2013 14:08:21 +0100
